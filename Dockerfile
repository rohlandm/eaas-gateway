FROM golang:1.12 AS build

RUN mkdir -p /build

COPY ./ /build/

WORKDIR /build

RUN go build

RUN mkdir -p /build-out
RUN cp /build/eaas-gateway /build-out/

FROM ubuntu:bionic

COPY --from=build /build-out/* /
ENTRYPOINT [ "./eaas-gateway" ]