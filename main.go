package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/rohlandm/eaas-gateway/k8s"
	"gitlab.com/rohlandm/eaas-gateway/routing"
)

func main() {

	// command line flags
	configPath := flag.String("config", "", "Path to a k8s config")
	flag.Parse()

	clientConfig, err := k8s.GetClient(*configPath)
	if err != nil {
		panic(err)
	}

	router := routing.NewRouter(clientConfig)

	port := os.Getenv("PORT") //Get port from .env file
	if port == "" {
		port = "8000" //localhost, default port
	}

	fmt.Println(port)

	log.Fatal(http.ListenAndServe(":"+port, router))
}
