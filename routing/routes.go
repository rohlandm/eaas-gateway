package routing

import (
	"net/http"

	"github.com/gorilla/mux"
	"k8s.io/client-go/kubernetes"
)

// Route contains everything needed to set up a route in the router
type Route struct {
	Name        string
	Method      string
	Pattern     string
	Handlerfunc http.HandlerFunc
}

// Routes is a List having Route as is elements
type Routes []Route

// NewRouter builds a router handling the routes defined in this file
func NewRouter(config *kubernetes.Clientset) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)

	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.Handlerfunc)
	}
	clusterConfig = config

	return router
}

var clusterConfig *kubernetes.Clientset

var routes = Routes{
	Route{
		"GetJobs",
		"GET",
		"/getjobs",
		GetJobs,
	},
	Route{
		"StartJob",
		"POST",
		"/startjob",
		StartJob,
	},
	Route{
		"GetJob",
		"GET",
		"/getjob/{EaaSUUID}",
		GetJob,
	},
	Route{
		"UpdateJob",
		"POST",
		"/updatejob/{EaaSUUID}",
		UpdateJob,
	},
	Route{
		"DeleteJob",
		"GET",
		"/deletejob/{EaaSUUID}",
		DeleteJob,
	},
	Route{
		"GetUtil",
		"GET",
		"/getutil",
		GetUtil,
	},
}
