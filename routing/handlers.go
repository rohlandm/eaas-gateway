package routing

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/rohlandm/eaas-gateway/k8s"
)

// GetJobs get all jobs currently running in the k8s cluster
func GetJobs(w http.ResponseWriter, r *http.Request) {
	if err := json.NewEncoder(w).Encode(k8s.GetJobs()); err != nil {
		panic(err)
	}
}

// GetJob returns the job for a given uuid
func GetJob(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	eaasUUIDString := vars["EaaSUUID"]

	eaasUUID, err := uuid.Parse(eaasUUIDString)
	if err != nil {
		errorMessage := err
		if err := json.NewEncoder(w).Encode(errorMessage); err != nil {
			panic(err)
		}
		return
	}

	job, jobErr := k8s.GetJob(eaasUUID)
	if err != nil {
		if err := json.NewEncoder(w).Encode(jobErr); err != nil {
			panic(err)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(job); err != nil {
		panic(err)
	}

}

// StartJob Start a job inside the k8s cluster
func StartJob(w http.ResponseWriter, r *http.Request) {

	var request k8s.JobRequest
	// limit request size to 1 MiB
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))

	if err != nil {
		log.Println("StartJob: Request to big")
	} else if err := r.Body.Close(); err != nil {
		log.Println("StartJob: Request corrupt")
	} else {
		if err := json.Unmarshal(body, &request); err != nil {
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(422) // unprocessable entity
			if err := json.NewEncoder(w).Encode(err); err != nil {
				panic(err)
			}
		}

		//start the job in the k8s cluster
		job, errK8s := k8s.StartJob(request, *clusterConfig)
		if errK8s != nil {
			if err := json.NewEncoder(w).Encode(errK8s); err != nil {
				panic(err)
			}
		}

		// return the started job
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		if err := json.NewEncoder(w).Encode(job); err != nil {
			panic(err)
		}
	}

}

// UpdateJob updates the status of a given job
func UpdateJob(w http.ResponseWriter, r *http.Request) {

	var update k8s.JobUpdate

	// limit request size
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		log.Println("UpdateJob: Request to big")
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Println("UpdateJob: Request corrupt")
		return
	}
	if err := json.Unmarshal(body, &update); err != nil {
		log.Println("UpdateJob: not a valid update request")
	}

	// update the job
	eaasUUID, err := uuid.Parse(update.EaasUUID)
	if err != nil {
		log.Println("UpdateJob: not a valid UUID")
		return
	}
	_, err = k8s.UpdateJob(eaasUUID, update.Status)
	if err != nil {
		log.Println("UpdateJob: Job not found")
		return
	}

}

// DeleteJob deletes an emulation job
func DeleteJob(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	eaasUUID, err := uuid.Parse(vars["EaaSUUID"])
	if err != nil {
		if err := json.NewEncoder(w).Encode("not a valid UUID"); err != nil {
			panic(err)
		}
	}
	err = k8s.DeleteJob(eaasUUID, *clusterConfig)
	if err != nil {
		if err := json.NewEncoder(w).Encode("job not found"); err != nil {
			panic(err)
		}
	}
	if err := json.NewEncoder(w).Encode("Job deleted"); err != nil {
		panic(err)
	}
}

// GetUtil returns utilization information
func GetUtil(w http.ResponseWriter, r *http.Request) {
	k8s.GetUtilization()
	if err := json.NewEncoder(w).Encode("get util"); err != nil {
		panic(err)
	}

}
