package k8s

import (
	"errors"

	"github.com/google/uuid"
)

// EaasJSON is the environment variable to start the qcow runner
type EaasJSON struct {
	Image string   `json:"image"`
	Args  []string `json:"args"`
	Disks []string `json:"disks"`
	Nics  int      `json:"nics"`
}

// JobRequest contains a name and the images needed to start a job
type JobRequest struct {
	Name          string
	EmulatorImage string
	DiskImage     string
	JSON          EaasJSON
}

// Job contains the original request, an assigned UID, the Status
type Job struct {
	Request    JobRequest
	EaasUUID   uuid.UUID
	Status     JobStatus
	HostIP     string
	HostDomain string
	Service    bool
	Ingress    bool
}

// JobUpdate contains the UID of the job to be updated as string and its new status
type JobUpdate struct {
	EaasUUID string
	Status   JobStatus
}

// JobStatus contains the Status of a Job
type JobStatus int

// list of possible statuses a job can have
const (
	FINISHED JobStatus = 0
	RUNNING  JobStatus = 1
	CRASHED  JobStatus = 2
	CREATED  JobStatus = 3
	UNKNOWN  JobStatus = 99
)

// Jobs is a map of all jobs
type Jobs map[uuid.UUID]Job

var activeJobs = Jobs{}

// GetJobs returns the map of active jobs
func GetJobs() Jobs {
	return activeJobs
}

// GetJob returns the job for a given ID
func GetJob(eaasUUID uuid.UUID) (Job, error) {
	val, ok := activeJobs[eaasUUID]
	if ok {
		return val, nil
	}
	return val, errors.New("Job not found")
}

// UpdateJob updates a job`s status
func UpdateJob(eaasUUID uuid.UUID, status JobStatus) (Job, error) {
	job, err := GetJob(eaasUUID)
	if err != nil {
		return job, err
	}

	job.Status = status
	activeJobs[eaasUUID] = job
	return job, nil
}
