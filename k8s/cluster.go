package k8s

import (
	"errors"
	"log"
	"time"

	"github.com/google/uuid"
	batchv1 "k8s.io/api/batch/v1"
	apiv1 "k8s.io/api/core/v1"
	v1beta1 "k8s.io/api/extensions/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	metrics "k8s.io/metrics/pkg/client/clientset/versioned"
)

// GetClient establishes a client for communication with a k8s cluster
func GetClient(cfgPath string) (*kubernetes.Clientset, error) {

	var config *rest.Config
	var err error

	// if an empty config path is given, assume the gateway itself is
	// hosted inside the desired k8s cluster and use its config
	if cfgPath == "" {
		log.Println("Using in Cluster config")
		config, err = rest.InClusterConfig()
	} else {
		log.Println("Using given config")
		config, err = clientcmd.BuildConfigFromFlags("", cfgPath)
	}
	if err != nil {
		return nil, err
	}

	return kubernetes.NewForConfig(config)

}

// StartJob starts a given job inside the k8s cluster
func StartJob(jobRequest JobRequest, client kubernetes.Clientset) (Job, error) {

	// create random eaasUUID
	eaasUUID := uuid.New()

	// create set of labels containing our UUID

	labels := make(map[string]string)
	labels["eaas-uuid"] = eaasUUID.String()

	jobsClient := client.BatchV1().Jobs("default")

	k8sJob := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      jobRequest.Name,
			Namespace: "default",
			Labels:    labels,
		},
		Spec: batchv1.JobSpec{
			Template: apiv1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:      jobRequest.Name,
					Namespace: "default",
					Labels:    labels,
				},
				Spec: apiv1.PodSpec{
					Containers: []apiv1.Container{
						{
							Name:            "emulator",
							Image:           "registry.gitlab.com/rohlandm/eaas-gateway/emu-base:latest",
							ImagePullPolicy: "Always",
							// add SYS_ADMIN Capability for FUSE
							SecurityContext: &apiv1.SecurityContext{
								Capabilities: &apiv1.Capabilities{
									Add: []apiv1.Capability{
										"SYS_ADMIN",
									},
								},
							},
							Env: []apiv1.EnvVar{
								{
									Name:  "EMULATORIMAGE",
									Value: jobRequest.EmulatorImage,
								},
								{
									Name:  "DISKIMAGE",
									Value: jobRequest.DiskImage,
								},
								{
									Name:  "EAASUUID",
									Value: eaasUUID.String(),
								},
								{
									Name:  "GATEWAYHOST",
									Value: "eaas-gateway",
								},
								{
									Name:  "GATEWAYPORT",
									Value: "8000",
								},
							},
						},
					},
					RestartPolicy: "Never",
				},
			},
		},
	}

	// create service for network access
	serviceClient := client.CoreV1().Services("default")

	k8sService := &apiv1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      jobRequest.Name,
			Namespace: "default",
			Labels:    labels,
		},
		Spec: apiv1.ServiceSpec{
			Selector: labels,
			Ports: []apiv1.ServicePort{
				{
					Name:       "xpra",
					Port:       8888,
					TargetPort: intstr.FromInt(8888),
				},
			},
		},
	}

	// create Ingress
	ingressClient := client.ExtensionsV1beta1().Ingresses("default")

	// map of known domain<->IP mappings for letsencrypt
	// TODO: exchange for some production grade mechanism
	dnsMap := make(map[string]string)
	dnsMap["192.52.34.251"] = "rancher1.eaas.rohlandm.de"
	dnsMap["192.52.34.255"] = "rancher2.eaas.rohlandm.de"
	dnsMap["192.52.35.16"] = "rancher3.eaas.rohlandm.de"
	dnsMap["192.52.34.16"] = "rancher4.eaas.rohlandm.de"

	_, err := jobsClient.Create(k8sJob)
	if err != nil {
		log.Println("could not create Job")
		return Job{}, err
	}

	time.Sleep(5 * time.Second)

	hostIP, err := getJobNodeIP(eaasUUID, client)
	if err != nil {
		return Job{}, err
	}

	hostDomain := dnsMap[hostIP]

	var k8sIngress v1beta1.Ingress

	if hostDomain == "" {

		k8sIngress = v1beta1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Name:      jobRequest.Name,
				Namespace: "default",
				Labels:    labels,
			},
			Spec: v1beta1.IngressSpec{
				Rules: []v1beta1.IngressRule{
					{
						IngressRuleValue: v1beta1.IngressRuleValue{
							HTTP: &v1beta1.HTTPIngressRuleValue{
								Paths: []v1beta1.HTTPIngressPath{
									{
										Path: "/" + eaasUUID.String(),
										Backend: v1beta1.IngressBackend{
											ServiceName: jobRequest.Name,
											ServicePort: intstr.FromInt(8888),
										},
									},
								},
							},
						},
					},
				},
			},
		}
	} else {

		annotations := make(map[string]string)
		annotations["kubernetes.io/tls-acme"] = "true"

		k8sIngress = v1beta1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Name:        jobRequest.Name,
				Namespace:   "default",
				Labels:      labels,
				Annotations: annotations,
			},
			Spec: v1beta1.IngressSpec{
				Rules: []v1beta1.IngressRule{
					{
						Host: hostDomain,
						IngressRuleValue: v1beta1.IngressRuleValue{
							HTTP: &v1beta1.HTTPIngressRuleValue{
								Paths: []v1beta1.HTTPIngressPath{
									{
										Path: "/" + eaasUUID.String(),
										Backend: v1beta1.IngressBackend{
											ServiceName: jobRequest.Name,
											ServicePort: intstr.FromInt(8888),
										},
									},
								},
							},
						},
					},
				},
				TLS: []v1beta1.IngressTLS{
					{
						Hosts:      []string{hostDomain},
						SecretName: hostDomain,
					},
				},
			},
		}
	}

	serviceStatus := true

	_, err = serviceClient.Create(k8sService)
	if err != nil {
		log.Println("could not create Service for Job, but Job was created")
		serviceStatus = false
		return Job{}, err
	}

	ingressStatus := true

	_, err = ingressClient.Create(&k8sIngress)
	if err != nil {
		log.Println("could not create Ingress, but Job and Service were created")
		ingressStatus = false
		return Job{}, err
	}

	var job = Job{
		Request:    jobRequest,
		EaasUUID:   eaasUUID,
		Status:     CREATED,
		HostIP:     hostIP,
		HostDomain: hostDomain,
		Service:    serviceStatus,
		Ingress:    ingressStatus,
	}

	activeJobs[eaasUUID] = job

	return job, nil

}

// CheckJob checks a Job for completion
// TODO: get this to work, either via the API or via a self made solution
func CheckJob(eaasUUID uuid.UUID, client kubernetes.Clientset) (status JobStatus, err error) {

	joblist, err := client.BatchV1().Jobs("default").List(metav1.ListOptions{
		LabelSelector: "eaas-uuid=" + eaasUUID.String(),
	})
	if err != nil {
		return UNKNOWN, err
	}

	job := joblist.Items[0]

	log.Println(job)

	return RUNNING, nil
}

// getJobNodeIP returns the Ip of the Node a Job is running on.
// This is needed to get direct access to XPRA
func getJobNodeIP(eaasUUID uuid.UUID, client kubernetes.Clientset) (ip string, err error) {

	// retrieve list of jobs
	joblist, err := client.BatchV1().Jobs("default").List(metav1.ListOptions{
		LabelSelector: "eaas-uuid=" + eaasUUID.String(),
	})
	if err != nil {
		return ip, err
	}

	job := joblist.Items[0]

	podlist, err := client.CoreV1().Pods("default").List(metav1.ListOptions{
		LabelSelector: "job-name=" + job.GetName(),
	})
	if err != nil {
		return ip, err
	}

	jobPod := podlist.Items[0]

	ip = jobPod.Status.HostIP

	return ip, nil

}

// DeleteJob deletes an emulation job from the cluster
func DeleteJob(eaasUUID uuid.UUID, client kubernetes.Clientset) (err error) {

	_, ok := activeJobs[eaasUUID]
	if ok {
		joblist, err := client.BatchV1().Jobs("default").List(metav1.ListOptions{
			LabelSelector: "eaas-uuid=" + eaasUUID.String(),
		})
		if err != nil {
			return err
		}
		job := joblist.Items[0]
		ingressClient := client.ExtensionsV1beta1().Ingresses("default")
		err = ingressClient.Delete(job.GetName(), &metav1.DeleteOptions{})
		if err != nil {
			log.Println("could not delete ingress")
			return err
		}
		serviceClient := client.CoreV1().Services("default")
		err = serviceClient.Delete(job.GetName(), &metav1.DeleteOptions{})
		if err != nil {
			log.Println("Could not delete service")
			return err
		}
		jobsclient := client.BatchV1().Jobs("default")
		err = jobsclient.Delete(job.GetName(), &metav1.DeleteOptions{})
		if err != nil {
			log.Println("Could not delete Job")
			return err
		}

		delete(activeJobs, eaasUUID)
		return nil
	}
	return errors.New("Job not found")
}

// GetUtilization returns the utilization of the cluster and warns about possible ressource shortages
func GetUtilization() (util string, err error) {

	config, err := rest.InClusterConfig()
	if err != nil {
		return "", err
	}
	metricsclient, err := metrics.NewForConfig(config)
	if err != nil {
		return "", err
	}

	nodemetrics, err := metricsclient.MetricsV1beta1().NodeMetricses().List(metav1.ListOptions{})
	if err != nil {
		return "", err
	}

	log.Println(nodemetrics.String())

	return "", nil
}
