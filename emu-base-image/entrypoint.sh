#!/bin/sh

# first demo: just sleep and update status

# change jobs status from created to running
curl -d '{"EAASUUID":"'$EAASUUID'", "Status": 1}' -H "Content-Type: application/json" -X POST http://$GATEWAYHOST:$GATEWAYPORT/updatejob/$EAASUUID

echo 'starting nginx'
service nginx start

sleep 300s

echo 'stopping nginx'
service nginx stop

# change jobs status from running to finished
curl -d '{"EAASUUID":"'$EAASUUID'", "Status": 0}' -H "Content-Type: application/json" -X POST http://$GATEWAYHOST:$GATEWAYPORT/updatejob/$EAASUUID


echo 'Job finished!'

# Delete the job
curl http://$GATEWAYHOST:$GATEWAYPORT/deletejob/$EAASUUID