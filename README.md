# eaas-gateway

Gateway for EaaS on Kubernetes

## Installation

The simplest way is to download the latest build from the Gitlab CI pipeline.

### Build from Source

#### Prerequisites

* Go 1.12

#### Building

1. ```git clone https://gitlab.com/rohlandm/eaas-gateway.git``` 
2. ```cd eaas-gateway```
3. ```go build```

The project uses [go modules](https://github.com/golang/go/wiki/Modules), so it is not necessary
to put it into ```$GOPATH```.


## Usage

Start the gateway server with the desired kubectl config:
```
./eaas-gateway -config=/path/to/config
```

If the ```-config``` flag is ommited or contains an empty string the server assumes that it is running inside
a k8s cluster itself and tries to read its configuration. In this case you have to assign sufficient clusterroles to its service account, e.g. "edit".

A set of example requests can be found in ```test/test.http```

## Rolebinding

After creation of the serrvice account for the gateway create a rolebinding allowing it to create jobs, e.g.:
```
kubectl create rolebinding eaas-jobspawner-edit --clusterrole=edit --serviceaccount=default:eaas-jobspawner --namespace=default
```